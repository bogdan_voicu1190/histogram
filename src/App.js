import './App.css';
import IndexPage from "./pages";
import { ApolloProvider } from '@apollo/client';
import { client } from "./services/apollo.service";

function App() {
  return (
    <div className="App">
        <ApolloProvider client={client}>
            <IndexPage/>
        </ApolloProvider>
    </div>
  );
}

export default App;
