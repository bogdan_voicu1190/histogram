import months from '../util/constants/months';
function Header({postsPerMonth}){
    return (<div className={'header'}>
        <h1>Posts per month</h1>
        <ul>
            {postsPerMonth.map((month,index)=>{
                return (
                    <li key={index}>{months[index]}:{month}</li>
                );
            })}
        </ul>
    </div>)
}
export default Header
