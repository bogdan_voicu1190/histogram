
import {useQuery} from "@apollo/client";
import {AxisLeft, AxisBottom} from '@visx/axis';
import {Group} from "@visx/group";
import {scaleBand, scaleLinear, scaleTime} from "@visx/scale";
import {extent} from "d3-array";
import {Bar} from '@visx/shape';
import {ALL_POSTS} from "../util/constants/gql.queries";
import Header from "./header";


function Histogram() {
    const {loading, error, data} = useQuery(ALL_POSTS);
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    const width = 800;
    const height = 800;
    const margin = {
        top: 60,
        bottom: 60,
        left: 80,
        right: 80,
    };
    const xMax = width - margin.left - margin.right;
    const yMax = height - margin.top - margin.bottom;

    const mapMonths = post => {
        const date = new Date(Number(post.createdAt));
        return date.getMonth();
    }

    const months = data.allPosts.map(mapMonths);
    const availableMonths = months.filter((elem, pos) => {
        return months.indexOf(elem) === pos;
    }).sort((a, b) => a - b);
    let postsPerMonth = {}
    data.allPosts.forEach(post => {
        const postDate = new Date(Number(post.createdAt));
        const postMonth = postDate.getMonth();
        if (!postsPerMonth[postMonth]) {
            postsPerMonth[postMonth] = [];
        }
        postsPerMonth[postMonth].push(post);

    });
    postsPerMonth = Object.keys(postsPerMonth).map((month) => postsPerMonth[month].length);
    const bandWidthScale = scaleBand({
        domain: availableMonths,
        range: [0, xMax],
        padding: 0.5
    });
    const bandHeightScale = scaleLinear({
        range: [yMax, 0],
        domain: [0, Math.max(...postsPerMonth)],
    })

    const x = d => new Date(Number(d.createdAt));
    const timeScale = scaleTime({
        range: [0, xMax],
        domain: extent(data.allPosts, x),
        nice: true
    });
    return (
        <div className="histogram">
            <Header postsPerMonth={postsPerMonth} />
            <svg width={width} height={height}>
                <Group top={margin.top} left={margin.left}>
                    <AxisLeft
                        scale={bandHeightScale}
                        top={0}
                        left={0}
                        label={'Posts'}
                        stroke={'#1b1a1e'}
                        tickTextFill={'#1b1a1e'}
                    />
                    <AxisBottom
                        scale={timeScale}
                        top={yMax}
                        label={'Months'}
                        stroke={'#1b1a1e'}
                        tickTextFill={'#1b1a1e'}
                    />
                    <Group top={-5}>
                        {availableMonths.map(month => {
                            const barWidth = bandWidthScale.bandwidth();
                            const barHeight = yMax - (bandHeightScale(postsPerMonth[month]));
                            const barX = bandWidthScale(month);
                            const barY = yMax - barHeight;
                            return (
                                <Bar
                                    key={`bar-${month}`}
                                    x={barX}
                                    y={barY}
                                    width={barWidth}
                                    height={barHeight}
                                    fill="rgba(23, 233, 217, .5)"
                                />
                            );
                        })}
                    </Group>
                </Group>
            </svg>
        </div>
    )
}

export default Histogram
