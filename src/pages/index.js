import Histogram from "../components/histogram";

function IndexPage() {
    return (
        <div className="histogram-page">
            <Histogram/>
        </div>
    );
}

export default IndexPage;
