import {ApolloClient, InMemoryCache} from "@apollo/client";
export const client = new ApolloClient({
    uri: 'https://fakerql.stephix.uk/graphql',
    cache: new InMemoryCache()
});


