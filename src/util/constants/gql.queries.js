import {gql} from "@apollo/client";

export const ALL_POSTS = gql`
    query AllPosts {
        allPosts(count: 500) {
           published
           id
           createdAt
        }         
    }
`;
